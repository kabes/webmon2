from gevent import monkey

VERSION = "2.9.2"
APP_NAME = "webmon2"

monkey.patch_all()
