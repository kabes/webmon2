# Copyright (c) Karol Będkowski, 2016-2023
# This file is part of webmon. Licence: GPLv3

"""
Access to binaries stored in database.
"""

from __future__ import annotations

import typing as ty

import psycopg
import structlog

from ._dbcommon import NotFoundError

if ty.TYPE_CHECKING:
    from ._db import DB

_LOG: structlog.stdlib.BoundLogger = structlog.getLogger(__name__)


def get(db: DB, datahash: str, user_id: int) -> tuple[bytes, str]:
    """Get binary data from db identified by `datahash` and `userid`.

    Args:
        datahash: hash of binary
        user_id: user id
    Raises:
        `NotFoundError`: binary not found
    Return:
        (binary data, content type)
    """
    _LOG.debug("db: get binary", datahash=datahash, user_id=user_id)

    if not user_id:
        raise ValueError("missing user_id")

    if datahash:
        with db.cursor() as cur:
            cur.execute(
                "SELECT data, content_type FROM binaries "
                "WHERE datahash=%s AND user_id=%s",
                (datahash, user_id),
            )
            res: tuple[bytes, str] | None = cur.fetchone()
            if res:
                return res

    raise NotFoundError


def save(
    db: DB,
    user_id: int,
    content_type: str,
    datahash: str,
    data: str | bytes | None,
) -> None:
    """
    Save binary in database.

    Args:
        db: database
            user_id: user id
        content_type: content type of data
        datahash: hash of data
        data: binary data
    """
    _LOG.debug(
        "db: save binary content_type: %r, type: %s",
        content_type,
        type(data),
        user_id=user_id,
        datahash=datahash,
    )

    if not user_id:
        raise ValueError("missing user_id")

    with db.cursor() as cur:
        cur.execute(
            "INSERT INTO binaries (datahash, user_id, data, content_type) "
            "VALUES (%s, %s, %s, %s) "
            "ON conflict (datahash, user_id) DO NOTHING",
            (datahash, user_id, psycopg.Binary(data), content_type),
        )


_REMOVE_UNUSED_SQL = """
DELETE FROM binaries b
WHERE user_id = %(user_id)s
    AND NOT EXISTS (
        SELECT NULL FROM entries e
        WHERE e.user_id = %(user_id)s AND e.icon = b.datahash
    )
    AND NOT EXISTS (
        SELECT NULL
        FROM source_state ss
        JOIN sources s ON s.id = ss.source_id
        WHERE s.user_id = %(user_id)s AND ss.icon = b.datahash
    )
"""


def remove_unused(db: DB, user_id: int) -> int:
    """
    Remove unused binaries for `user_id`

    Returns:
        number of deleted entries
    """
    if not user_id:
        raise ValueError("missing user_id")

    with db.cursor() as cur:
        cur.execute(_REMOVE_UNUSED_SQL, {"user_id": user_id})
        return ty.cast(int, cur.rowcount)


_CLEAN_ENTRIES_SQL = """
UPDATE entries e
SET icon=NULL
WHERE icon IS NOT NULL
    AND NOT EXISTS (
        SELECT NULL
        FROM binaries b
        WHERE b.datahash=e.icon AND b.user_id=e.user_id
    )
"""

_CLEAN_SOURCE_STATE_SQL = """
UPDATE source_state ss
SET icon=NULL
WHERE icon IS NOT NULL
    AND NOT EXISTS (
        SELECT NULL
        FROM binaries b
        JOIN sources s ON b.user_id = s.user_id
        WHERE b.datahash=ss.icon AND ss.source_id = s.id
    )
"""


def clean_sources_entries(db: DB) -> tuple[int, int]:
    """
    Clean old binaries.

    Args:
        db: database
    Return:
        (deleted states count, deleted entries count)
    """
    with db.cursor() as cur:
        cur.execute(_CLEAN_SOURCE_STATE_SQL)
        states_num = cur.rowcount

    with db.cursor() as cur:
        cur.execute(_CLEAN_ENTRIES_SQL)
        entries_num = cur.rowcount

    return (states_num, entries_num)
