# Copyright © 2019 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.

"""
Select entries by matching text.
"""

from flask_babel import lazy_gettext

from webmon2 import model

from ._abstract import AbstractFilter


class Sort(AbstractFilter):
    """Sort entries"""

    name = "sort"
    short_info = lazy_gettext("Sort elements")
    long_info = lazy_gettext("Sort elements by title and content")

    def filter(
        self,
        entries: model.Entries,
        prev_state: model.SourceState,  # noqa:ARG002
        curr_state: model.SourceState,  # noqa:ARG002
    ) -> model.Entries:
        return sorted(entries, key=lambda e: (e.title, e.content))

    def _filter(self, entry: model.Entry) -> model.Entries:
        raise NotImplementedError
