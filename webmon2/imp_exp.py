# Copyright © 2019 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
# ruff: noqa: ANN401
"""
Import/Export sources & groups.
"""

from __future__ import annotations

import json
import typing as ty

from webmon2 import database, model


def dump_object(
    obj: ty.Any, attrs: ty.Iterable[str] | None = None
) -> dict[str, ty.Any]:
    if not attrs and hasattr(obj, "__slots__"):
        attrs = obj.__slots__

    if not attrs and hasattr(obj, "__dataclass_fields__"):
        attrs = obj.__dataclass_fields__

    if not attrs:
        return {}

    return {attr: getattr(obj, attr) for attr in attrs}


def _dump_groups(
    groups: ty.Iterable[model.SourceGroup],
) -> ty.Iterable[dict[str, ty.Any]]:
    for group in groups:
        yield dump_object(group, ("id", "name", "user_id"))


def _dump_sources(
    sources: ty.Iterable[model.Source],
) -> ty.Iterable[dict[str, ty.Any]]:
    for source in sources:
        yield dump_object(
            source,
            (
                "id",
                "group_id",
                "kind",
                "name",
                "interval",
                "settings",
                "filters",
                "user_id",
                "status",
            ),
        )


def dump_export(db: database.DB, user_id: int) -> str:
    data = {
        "groups": list(_dump_groups(database.groups.get_all(db, user_id))),
        "settings": list(
            map(dump_object, database.settings.get_all(db, user_id))
        ),
        "sources": list(_dump_sources(database.sources.get_all(db, user_id))),
    }
    return json.dumps(data)


def fill_object(
    obj: ty.Any,
    data: dict[str, ty.Any],
    attrs: ty.Iterable[str] | None = None,
) -> None:
    if not attrs:
        attrs = obj.__slots__

    if not attrs:
        return

    for attr in attrs:
        if attr in data and attr != "id":
            setattr(obj, attr, data[attr])


def dump_import(db: database.DB, user_id: int, data_str: str) -> None:
    data = json.loads(data_str)
    if not data:
        raise RuntimeError("no data")

    groups_map: dict[int, int] = {}
    for group in data.get("groups") or []:
        try:
            grp = database.groups.find(db, user_id, group["name"])
        except database.NotFoundError:
            grp = model.SourceGroup(
                user_id=user_id,
                name=group["name"],
                feed=group.get("feed"),
                mail_report=group.get("mail_report"),
            )
            grp = database.groups.save(db, grp)

        assert grp.id
        groups_map[group["id"]] = grp.id

    sources_map = {}
    for source in data.get("sources") or []:
        src = model.Source(
            user_id=user_id,
            group_id=groups_map[source["group_id"]],
            name=source["name"],
            kind=source["kind"],
        )
        src.status = model.SourceStatus(source["status"])
        fill_object(
            src,
            source,
            (
                "group_id",
                "interval",
                "settings",
                "filters",
                "user_id",
            ),
        )
        src = database.sources.save(db, src)
        sources_map[source["id"]] = src.id
