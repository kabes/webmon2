"""
Main functions.

Copyright (c) Karol Będkowski, 2016-2024

This file is part of webmon.
Licence: GPLv2+
"""

from __future__ import annotations

import argparse
import importlib.util
import locale
import os.path
import signal
import sys
import typing as ty
from contextlib import suppress
from pathlib import Path

import structlog
from werkzeug.serving import is_running_from_reloader

try:
    import stackprinter

    stackprinter.set_excepthook(style="color")
except ImportError:
    stackprinter = None

    with suppress(ImportError):
        from rich.traceback import install

        install()

with suppress(ImportError):
    import icecream

    icecream.install()

try:
    import sdnotify

    HAS_SDNOTIFY = True
except ImportError:
    HAS_SDNOTIFY = False

with suppress(ImportError):
    import setproctitle


from . import (
    APP_NAME,
    VERSION,
    cli,
    conf,
    database,
    logging_setup,
    web,
    worker,
)

if ty.TYPE_CHECKING:
    from configparser import ConfigParser


_LOG: structlog.stdlib.BoundLogger = structlog.getLogger("main")
_DEFAULT_DB_FILE = "~/.local/share/" + APP_NAME + "/" + APP_NAME + ".db"
_SDN = sdnotify.SystemdNotifier() if HAS_SDNOTIFY else None
_SDN_WATCHDOG_INTERVAL = 15


def _parse_options() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description=APP_NAME + " " + VERSION)
    parser.add_argument(
        "-s",
        "--silent",
        action="store_true",
        help="show only errors and warnings",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="show additional information",
    )
    parser.add_argument(
        "-d", "--debug", action="store_true", help="print debug information"
    )
    parser.add_argument("--log-fmt", help="log format (console, logfmt)")

    parser.add_argument(
        "-c",
        "--conf",
        type=argparse.FileType("r"),
        help="configuration file name",
        dest="conf",
    )
    parser.add_argument(
        "--database",
        help="database connection string",
    )

    subparsers = parser.add_subparsers(help="Commands", dest="cmd")
    subparsers.add_parser(
        "abilities", help="show available filters/sources/comparators"
    )

    subparsers.add_parser("update-schema", help="update database schema")
    subparsers.add_parser("shell", help="launch IPython shell")

    parser_mig = subparsers.add_parser(
        "migrate", help="migrate sources from file"
    )
    parser_mig.add_argument(
        "-f",
        "--filename",
        help="migrate sources from file",
        dest="migrate_filename",
        required=True,
    )
    parser_mig.add_argument(
        "-u",
        "--user",
        help="target user login",
        dest="migrate_user",
        required=True,
    )

    parser_users_sc = subparsers.add_parser(
        "users", help="manage users"
    ).add_subparsers(help="user commands", dest="subcmd", required=True)

    parser_users_add = parser_users_sc.add_parser("add", help="add user")
    parser_users_add.add_argument("-l", "--login", required=True)
    parser_users_add.add_argument("-p", "--password", required=True)
    parser_users_add.add_argument(
        "--admin",
        action="store_true",
        default=False,
        help="set admin role for user",
    )

    parser_users_cp = parser_users_sc.add_parser(
        "passwd", help="change user password"
    )
    parser_users_cp.add_argument("-l", "--login", required=True)
    parser_users_cp.add_argument("-p", "--password", required=True)

    parser_users_rtotp = parser_users_sc.add_parser(
        "remove_totp", help="remove two factor authentication for user"
    )
    parser_users_rtotp.add_argument("-l", "--login", required=True)

    parser_serve = subparsers.add_parser("serve", help="Start application")

    parser_serve.add_argument(
        "--app-root",
        help="root for url patch (for reverse proxy)",
        dest="web_app_root",
    )
    parser_serve.add_argument(
        "--workers", type=int, help="number of background workers"
    )
    parser_serve.add_argument(
        "--address",
        type=str,
        help="web interface listen address",
        dest="web_address",
    )
    parser_serve.add_argument(
        "--port",
        type=str,
        help="web interface listen port",
        dest="web_port",
    )
    parser_serve.add_argument(
        "--smtp-server-address",
        help="smtp server address",
        dest="smtp_server_address",
    )
    parser_serve.add_argument(
        "--smtp-server-port", help="smtp server port", dest="smtp_server_port"
    )
    parser_serve.add_argument(
        "--smtp-server-ssl",
        help="enable ssl for smtp serve",
        action="store_true",
        dest="smtp_server_ssl",
    )
    parser_serve.add_argument(
        "--smtp-server-starttls",
        help="enable starttls for smtp serve",
        action="store_true",
        dest="smtp_server_starttls",
    )
    parser_serve.add_argument(
        "--smtp-server-from",
        help="email address for webmon",
        dest="smtp_server_from",
    )
    parser_serve.add_argument(
        "--smtp-server-login",
        help="login for smtp authentication",
        dest="smtp_server_login",
    )
    parser_serve.add_argument(
        "--smtp-server-password",
        help="password for smtp authentication",
        dest="smtp_server_password",
    )

    parser_wc = subparsers.add_parser(
        "write-config", help="write default configuration file"
    )
    parser_wc.add_argument(
        "-f",
        "--filename",
        help="destination filename",
        default="~/.config/webmon2/webmon2.ini",
        dest="conf_filename",
        required=True,
    )

    return parser.parse_args()


def _load_user_classes() -> None:
    users_scripts_dir = Path("~/.local/share/", APP_NAME).expanduser()
    if not users_scripts_dir.is_dir():
        return

    for fname in os.listdir(users_scripts_dir):
        fpath = Path(users_scripts_dir, fname)
        if (
            fpath.is_file()
            and fname.endswith(".py")
            and not fname.startswith("_")
        ):
            _LOG.info("main: loading user classes from %r", fpath)
            modname = fname[:-3]
            try:
                spec = importlib.util.spec_from_file_location(modname, fpath)
                if spec:
                    module = importlib.util.module_from_spec(spec)
                    sys.modules[modname] = module
                    spec.loader.exec_module(module)  # type: ignore

            except ImportError as err:
                _LOG.error(
                    "main: importing user classes from file '%s' error",
                    fpath,
                    error=err,
                )


def _check_libraries() -> None:
    # pylint: disable=unused-import,import-outside-toplevel
    # pylint: disable=c-extension-no-member
    try:
        from lxml import etree

        _LOG.debug("main: etree version: %s", etree.__version__)
    except ImportError:
        _LOG.info("main: missing lxml library")

    try:
        import cssselect

        _LOG.debug("main: cssselect version: %s", cssselect.__version__)
    except ImportError:
        _LOG.info("main: missing cssselect library")

    try:
        import html2text

        _LOG.debug("main: html2text version: %s", html2text.__version__)
    except ImportError:
        _LOG.info("main: missing html2text library")

    try:
        import markdown2

        _LOG.debug("main: markdown2 version: %s", markdown2.__version__)
    except ImportError:
        _LOG.info("main: missing markdown2 library")

    try:
        import yaml

        _LOG.debug("main: yaml version: %s", yaml.__version__)
    except ImportError:
        _LOG.info("main: missing yaml library")

    try:
        import requests

        _LOG.debug("main: requests version: %s", requests.__version__)
    except ImportError:
        _LOG.info("main: missing requests library")

    try:
        import feedparser

        _LOG.debug("main: feedparser version: %s", feedparser.__version__)
    except ImportError:
        _LOG.info("main: missing feedparser library")

    try:
        import github3

        _LOG.debug("main: github3.py version: %s", github3.__version__)
    except ImportError:
        _LOG.info("main: missing github3 library")

    try:
        import flask_minify

        _LOG.debug(
            "main: flask_minify version: %s",
            flask_minify.__version__,
        )
    except ImportError:
        _LOG.info("main: missing optional flask_minify library")


def _sd_watchdog(_signal: ty.Any, _frame: ty.Any) -> None:  # noqa: ANN401
    assert _SDN
    _SDN.notify("WATCHDOG=1")
    signal.alarm(_SDN_WATCHDOG_INTERVAL)


def _load_conf(args: argparse.Namespace) -> ConfigParser:
    app_conf: ConfigParser | None
    if args.conf:
        app_conf = conf.load_conf(args.conf)
    else:
        app_conf = conf.try_load_user_conf()

    if not app_conf:
        _LOG.debug("main: loading default conf")
        app_conf = conf.default_conf()

    app_conf = conf.update_from_args(app_conf, args)
    _LOG.debug("main: app_conf", conf=conf.conf_items(app_conf))
    return app_conf


def _serve(args: argparse.Namespace, app_conf: ConfigParser) -> None:
    if (
        not is_running_from_reloader()
        and app_conf.getint("main", "workers", fallback=2) > 0
    ):
        if HAS_SDNOTIFY and _SDN:
            _SDN.notify("STATUS=starting workers")

        cworker = worker.CheckWorker(
            app_conf, debug=args.debug, sdn=_SDN if HAS_SDNOTIFY else None
        )
        cworker.start()

    if HAS_SDNOTIFY and _SDN:
        _SDN.notify("STATUS=running")
        _SDN.notify("READY=1")

    try:
        web.start_app(args, app_conf)
    except Exception as err:  # pylint: disable=broad-except
        if stackprinter:
            stackprinter.show()

        _LOG.error("main: start app error: %s", error=err)

    if HAS_SDNOTIFY and _SDN:
        _SDN.notify("STOPPING=1")


def _update_schema(app_conf: ConfigParser) -> None:
    if is_running_from_reloader():
        _LOG.error("main: cannot update schema when running from reloader")
    else:
        _LOG.info("main: update schema starting...")
        database.DB.initialize(app_conf.get("main", "database"), True, 1, 5)


def main() -> None:
    """Main function."""

    with suppress(NameError):
        setproctitle.setproctitle("webmon2")

    if HAS_SDNOTIFY and _SDN:
        _SDN.notify("STATUS=starting")
        signal.signal(signal.SIGALRM, _sd_watchdog)
        signal.alarm(_SDN_WATCHDOG_INTERVAL)

    with suppress(locale.Error):
        locale.setlocale(locale.LC_ALL, locale.getlocale())

    args = _parse_options()
    logging_setup.setup(args.log_fmt, args.debug, args.silent)

    _check_libraries()
    _load_user_classes()

    if args.cmd == "abilities":
        cli.show_abilities()
        return

    app_conf = _load_conf(args)
    if not conf.validate(app_conf):
        _LOG.error("main: app_conf validation error")
        return

    if args.cmd == "update-schema":
        _update_schema(app_conf)
        return

    if HAS_SDNOTIFY and _SDN:
        _SDN.notify("STATUS=init-db")

    database.DB.initialize(
        app_conf.get("main", "database"),
        False,
        app_conf.getint("main", "db_pool_min", fallback=2),
        app_conf.getint("main", "db_pool_max", fallback=20),
    )

    if cli.process_cli(args, app_conf):
        return

    if args.cmd == "serve":
        _serve(args, app_conf)
        return

    _LOG.error("main: missing command")


if __name__ == "__main__":
    main()
