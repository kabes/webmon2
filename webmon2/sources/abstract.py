# Copyright © 2019 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.

"""
Abstract source definition
"""

import abc
import typing as ty

import requests
import structlog

from webmon2 import common, model


class AbstractSource(abc.ABC):
    """Abstract/Base class for all sources"""

    # name used in configuration
    name: str | None = None
    params: tuple[common.SettingDef, ...] = ()
    short_info = ""
    long_info = ""

    AGENT = (
        "Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0"
    )

    def __init__(
        self, source: model.Source, sys_settings: model.ConfDict
    ) -> None:
        super().__init__()
        self._source = source
        self._log = structlog.get_logger(__name__).bind(source_id=source.id)

        # when _updated_source is set, after loading this configuration
        # overwrite data in database
        self._updated_source = None  # type: ty.Optional[model.Source]

        self._conf: model.ConfDict = common.apply_defaults(
            {param.name: param.default for param in self.params},
            sys_settings,
            source.settings,
        )
        self._log.debug("source: configuration", conf=self._conf)

    def __str__(self) -> str:
        return f"<{self.__class__.__name__} {self._source} {self._conf!r}>"

    def validate(self) -> None:
        for name, error in self.validate_conf(self._conf):
            errmsg = f"parameter {name} error {error}"
            raise common.ParamError(errmsg)

    @property
    def updated_source(self) -> ty.Optional[model.Source]:
        """
        Get updated source configuration (if was updated).
        """
        return self._updated_source

    @classmethod
    def validate_conf(
        cls: ty.Type[ty.Self], *confs: model.ConfDict
    ) -> ty.Iterable[tuple[str, str]]:
        """Validate input configuration.
        Returns  iterable of (<parameter>, <error>)
        """
        for param in cls.params or []:
            if not param.required:
                continue

            values = [
                conf[param.name] for conf in confs if conf.get(param.name)
            ]
            if not values:
                yield (param.name, f'missing parameter "{param.description}"')
                continue

            if not param.validate_value(values[0]):
                yield (
                    param.name,
                    f'invalid value {values[0]!r} for "{param.description}"',
                )

    @classmethod
    def upgrade_conf(
        cls: ty.Type[ty.Self], source: model.Source
    ) -> model.Source:
        """
        Update source configuration; apply some additional data.
        `upgrade_conf` is launched before save source and may be run on source
        init (defined in source).
        """
        return source

    @abc.abstractmethod
    def load(
        self, state: model.SourceState
    ) -> tuple[model.SourceState, model.Entries]:
        """Load data; return list of items (Result)."""
        raise NotImplementedError

    def _load_binary(
        self,
        url: str,
        only_images: bool = True,
        session: ty.Optional[requests.Session] = None,
    ) -> ty.Optional[tuple[str, bytes]]:
        """
        Load binary from given url.

        Args:
            url: url of binary to load
            only_images: if true accept only image-like files
            session: optional requests.Session to reuse

        Return:
            None on error
            (<content type>, <binary data>) on success
        """
        log = self._log.bind(url=url)
        log.debug("source: load_binary: loading")
        # reuse requests.session if available
        req = session.request if session else requests.request

        headers = {
            "User-agent": self.AGENT,
            "Accept-Language": "en-US;q=0.7,en;q=0.3",
        }
        headers.update(
            common.parse_str_to_headers(self._conf.get("http_headers"))
        )

        try:
            response = req(
                url=url,
                method="GET",
                headers=headers,
                allow_redirects=True,
                timeout=30,
            )
            if response:
                response.raise_for_status()
                if response.status_code == 200:  # noqa:PLR2004
                    if only_images and not _check_content_type(
                        response, _IMAGE_TYPES
                    ):
                        log.debug(
                            "source: load_binary: load skipped due "
                            "unacceptable content type: %s",
                            response.headers["Content-Type"],
                            url=url,
                        )
                        return None

                    return response.headers["Content-Type"], response.content

                log.debug(
                    "source: load_binary: invalid response",
                    url=url,
                    status=response.status_code,
                    error=response.text,
                )
        except requests.exceptions.RequestException as err:
            log.debug("source: load_binary: load error", url=url, error=err)

        except Exception as err:  # pylint: disable=broad-except
            log.exception(
                "source: load_binary: load error", url=url, error=err
            )

        return None

    @classmethod
    def get_param_types(cls: ty.Type[ty.Self]) -> dict[str, str]:
        return {param.name: param.type for param in cls.params}  # type: ignore

    @classmethod
    def get_param_defaults(cls: ty.Type[ty.Self]) -> dict[str, ty.Any]:
        return {param.name: param.default for param in cls.params}

    @classmethod
    def to_opml(
        cls: ty.Type[ty.Self], source: model.Source
    ) -> dict[str, ty.Any]:
        raise NotImplementedError

    @classmethod
    def from_opml(
        cls: ty.Type[ty.Self], opml_node: dict[str, ty.Any]
    ) -> ty.Optional[model.Source]:
        raise NotImplementedError


_IMAGE_TYPES = {
    "image/png",
    "image/x-icon",
    "image/vnd.microsoft.icon",
}


def _check_content_type(
    response: requests.Response, accepted: ty.Iterable[str]
) -> bool:
    content_type = response.headers["Content-Type"]
    return content_type in accepted
