# Copyright © 2019 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.

"""
Local file source
"""

from __future__ import annotations

import datetime
import os
import pathlib
import typing as ty

from flask_babel import lazy_gettext

from webmon2 import common, model

from .abstract import AbstractSource


class FileSource(AbstractSource):
    """Load data from local file"""

    name = "file"
    short_info = lazy_gettext("Data from local file")
    long_info = lazy_gettext(
        'Source check local, text file defined by "Full file patch" setting'
    )
    params = (
        common.SettingDef(
            "filename", lazy_gettext("Full file patch"), required=True
        ),
    )

    def load(
        self, state: model.SourceState
    ) -> tuple[model.SourceState, model.Entries]:
        """Return one part - page content."""

        fname = self._conf["filename"]
        self._log.debug("file source: load start", file=fname)
        ifile = pathlib.Path(fname)

        if not ifile.is_file():
            return state.new_error("no file"), []

        if state.last_update:
            fid = os.open(ifile, os.O_RDONLY)
            stat = os.fstat(fid)
            file_change = stat.st_mtime
            os.close(fid)

            if file_change <= state.last_update.timestamp():
                return state.new_not_modified(), []

        try:
            content = ifile.read_text(encoding="UTF-8")

            self._log.debug("file source: content loaded", content=content)

            entry = model.Entry.for_source(self._source)
            entry.updated = entry.created = datetime.datetime.now(datetime.UTC)
            entry.status = (
                model.EntryStatus.UPDATED
                if state.last_update
                else model.EntryStatus.NEW
            )
            entry.title = self._source.name
            entry.url = fname
            entry.content = content
            entry.set_opt("content-type", "plain")
            new_state = state.new_ok()
            assert self._source.interval is not None
            new_state.next_update = datetime.datetime.now(
                datetime.UTC
            ) + datetime.timedelta(
                seconds=common.parse_interval(self._source.interval)
            )
        except OSError as err:
            return state.new_error(str(err)), []
        else:
            return new_state, [entry]

    @classmethod
    def to_opml(
        cls: ty.Type[ty.Self], source: model.Source
    ) -> dict[str, ty.Any]:
        assert source.settings is not None
        return {
            "text": source.name,
            "title": source.name,
            "type": cls.name,
            "xmlUrl": "file://" + source.settings["filename"],
            "htmlUrl": "file://" + source.settings["filename"],
        }

    @classmethod
    def from_opml(
        cls: ty.Type[ty.Self], opml_node: dict[str, ty.Any]
    ) -> model.Source | None:
        url = opml_node.get("htmlUrl") or opml_node["xmlUrl"]
        if not url or not url.startswith("file://"):
            raise ValueError("missing xmlUrl")

        name = opml_node.get("text") or opml_node["title"]
        if not name:
            raise ValueError("missing text/title")

        filename = url[7:]
        src = model.Source(
            user_id=0,
            group_id=0,
            kind=cls.name,
            name=name,
        )
        src.settings = {"filename": filename}
        return src
